import {vector} from "https://unpkg.com/@flatten-js/core?module";
import Flatten from 'https://unpkg.com/@flatten-js/core?module';

export default class utilities
{
constructor(){}
InsertPoints(vec1, vec2, noOfPoints)
{
	
	let v1 = new Flatten.Vector();
	v1.x = vec1[0];
	v1.y = vec1[1];
	
	let v2 = new Flatten.Vector();
	v2.x = vec2[0];
	v2.y = vec2[1];
	
	let alpha = 1/noOfPoints;
	
	let interpolatedArray = [];
	for (let i=0; i<noOfPoints; i++)
	{
		let vA = v2.multiply(i*alpha);
		let vB = v1.multiply(1-i*alpha);
		let vR = vA.add(vB);
		interpolatedArray.push([vR.x, vR.y]);
	}
	interpolatedArray.push([v2.x, v2.y]);
	return interpolatedArray;
}
};
