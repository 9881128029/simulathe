import {point, circle, polygon, arc, segment, box} from "https://unpkg.com/@flatten-js/core?module";
import Flatten from 'https://unpkg.com/@flatten-js/core?module';
let {unify, subtract} = Flatten.BooleanOperations;

export default class StandardOD_Tool
{
	constructor(A, BA, IC)
	{
		this.A = A;
		this.BA = BA;
		this.IC = IC;
	}
	
	GetToolPoints()
	{
		var points = [];
		points[0] = point(0, 0); // Point 1
		var l = (this.IC/2)/(Math.tan(this.A/2));
		var B = ((Math.PI*2)- (2*this.A))/2;
		var l1 = (this.IC/2)/(Math.tan(B/2));
		var length = l + l1;
		
		points[1] = point(length, 0); // Point 2
		points[2] = points[1].rotate((Math.PI*2)-(this.A)); // Point 3
		points[3] = point(points[2].x + length, points[2].y); // Point 4
		
		
		
/*		var temp;
		temp = points[2];
		points[2] = points[3];
		points[3] = temp;*/
		
		let tool = [];
		let rotatedToolByBA = [];
		for (var i=0; i<4; i++)
		{
			tool[i] = point(points[i].x , points[i].y);
			var pt = tool[i].rotate((Math.PI*2)-(this.BA));
		    rotatedToolByBA[i] = [pt.x, pt.y];
		    
		}
		
		 let rotatedToolOUT = []
		rotatedToolOUT[0] = rotatedToolByBA[2];
		rotatedToolOUT[1] = rotatedToolByBA[0];
		rotatedToolOUT[2] = rotatedToolByBA[1];
		rotatedToolOUT[3] = rotatedToolByBA[3];
		return rotatedToolOUT;
	}
}

/* function main()
{
	
	let tool = Test_3();
	let s0 = segment(tool[0], tool[1]);
	let s1 = segment(tool[1], tool[2]); 
	let s2 = segment(tool[2], tool[3]);
	let s3 = segment(tool[3], tool[0]); 

    document.getElementById("stage").innerHTML = s0.svg() + s1.svg() + s2.svg() + s3.svg() ;
}
main();

function Test_1()
{
	var IC = 50;
	var A = (Math.PI/4);
	var BA = Math.PI/10;
	var obj = new StandardOD_Tool(A, BA, IC);
	var tool = obj.GetToolPoints();
	return tool;
}

function Test_2()
{
	var IC = 60;
	var A = (Math.PI/16);
	var BA = Math.PI/4;
	var obj = new StandardOD_Tool(A, BA, IC);
	var tool = obj.GetToolPoints();
	return tool;
}

function Test_3()
{
	var IC = 50;
	var A = (Math.PI/2);
	var BA = Math.PI/10;
	var obj = new StandardOD_Tool(A, BA, IC);
	var tool = obj.GetToolPoints();
	return tool;
} */

