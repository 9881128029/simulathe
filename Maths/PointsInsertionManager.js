import {point, circle, polygon, arc, segment, vector} from "https://unpkg.com/@flatten-js/core?module";
import Flatten from 'https://unpkg.com/@flatten-js/core?module';

let {unify, subtract} = Flatten.BooleanOperations;

export default class PointsInsertionManager
{
	constructor(interval)
	{
		this.interval = interval;
	}
	
	 ProcessPath(arrOfPoints)
	 {	
			
			let vectors = [];
			let newArrPathPoints = [];
			  for (var i = 0; i<arrOfPoints.length-1; i++)
				{
				  var p1 = point(arrOfPoints[i][0], arrOfPoints[i][1]);
				  var p2 = point(arrOfPoints[i+1][0], arrOfPoints[i+1][1]);
				  
				  var currVector = new Flatten.Vector(p2, p1);
				  vectors.push(currVector);
				  newArrPathPoints.push(p1);
				  
				  while (currVector.length > this.interval)
					{
						      var vecNorm = currVector.normalize();
						      var vecNormMagnitude = vecNorm.multiply(this.interval);
						      var pointAtFixedDistanceFromP1 = 
						      point(p1.x - vecNormMagnitude.x, p1.y - vecNormMagnitude.y);
						      newArrPathPoints.push(pointAtFixedDistanceFromP1);
						      p1 = pointAtFixedDistanceFromP1;
						      currVector = new Flatten.Vector(p2, p1);
				    } 
				  newArrPathPoints.push(p2); 
				}
			  
			  return newArrPathPoints;
	 }
}
