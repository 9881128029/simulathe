import {point, circle, polygon, arc, segment, box} from "https://unpkg.com/@flatten-js/core?module";
import Flatten from 'https://unpkg.com/@flatten-js/core?module';

let {unify, subtract} = Flatten.BooleanOperations;

export default class cutPolygon
{
	constructor (baseBoxArr, toolDefinition, toolAngle)
	{
		
		this.baseBox = box(baseBoxArr[0], baseBoxArr[1],baseBoxArr[2], baseBoxArr[3]);
		this.baseArray = GetArrayOfBaseBox(this.baseBox, true);
		this.baseBlock = GetPolygon(this.baseArray);
		this.toolDefinition = toolDefinition;
		this.toolAngle = toolAngle;
		this.maxDimension = GetMaxDimension(this.baseBox);
		
		//this.intermedietePolygons = [];
	}

	GetFinalCutPath(arrOfToolPath)
	{ 
		var flag1 = CheckValidPolygon(arrOfToolPath); 
		var flag2 = CheckValidPolygon(this.toolDefinition);
		var flag3 = true;
		
		if(flag1 == true && flag2 == true )
		{
		   //var flag3 = CheckWindingFor_ToolPath_ToolDefinition(this.baseArray, this.toolDefinition);
		   
		   if(flag3==true)
		   {	   
		       let baseBlockSubtracted = this.baseBlock;
		       for (var i=0; i<arrOfToolPath.length-1; i++)
			       {
			         baseBlockSubtracted = GetCutPolygonForTwoPoints([arrOfToolPath[i], arrOfToolPath[i+1]], 
					         this.toolDefinition, baseBlockSubtracted, this.toolAngle, this.maxDimension);
			         
			         
			       }
	
		       
		     var polygonVerticesArray = [];
		     var islands =  baseBlockSubtracted.splitToIslands();
		     for (var i=0; i<islands[0].vertices.length; i++)
		     {
		    	 polygonVerticesArray.push([islands[0].vertices[i].x, islands[0].vertices[i].y]);
		     }
		     var polygonVectorPoints = ArrayFormatting(polygonVerticesArray);
		     //document.getElementById("stage").innerHTML += islands[0].svg();
		     /*return polygonVectorPoints;
		     */
		     this.baseBlock = baseBlockSubtracted;
		     //return baseBlockSubtracted;
		     return polygonVectorPoints;
		   }
		   
		   else 
		   {
			   console.log("Direction of winding of the Tool Path and Tool Definition has to be same.")  
		   }
		}
		
		else if(flag1==true && flag2==false )
		{
			console.log("Invalid Tool Definition.")
		}
		
		else if(flag1==false && flag2==true)
		{
			console.log("Invalid Tool Path.")
		}
		
		else 
		{
			console.log("Invalid Tool Path and Invalid Tool Definition")
		}
	}
}

function CheckValidPolygon(arrOfPoints)
{
	let polygon = GetPolygon(arrOfPoints);
	//if( polygon.vertices.length == 0 ) 
	//if(polygon.area() == 0)
	if(polygon.isValid())
	{
		return true;
	}
	else 
	{
		return false;
	}
}

function GetUnifiedPolygons(arrOfPolygons)
{
	var unifiedPolygons = arrOfPolygons[0];

	for(var j=0; j<arrOfPolygons.length-1; j++)
	{
		unifiedPolygons = unify(unifiedPolygons, arrOfPolygons[j+1]);
	}	
	return unifiedPolygons;
}

function GetPolygon(arrOfPoints)
{
	// returns a polygon
	let p = new Flatten.Polygon();
	var points = [];
	const size= arrOfPoints.length;
	for (var i=0; i<size; i++)
	{
		points[i] = point(arrOfPoints[i][0], arrOfPoints[i][1]);
	}
	p.addFace(points);
	return p;
}

function RotateTool(toolDefinition, toolAngle)
{
	let rotated = [];
	let rotatedToolDefinition = [];
	for(var j=0; j<toolDefinition.length; j++)
	{
		rotated[j]= point(toolDefinition[j]).rotate(toolAngle); 
		rotatedToolDefinition[j] = [rotated[j].x, rotated[j].y];
	}
	return rotatedToolDefinition;
}

function GetArrayOfBaseBox(baseBox,  clockwise)
{
	if(clockwise)
		return [ [baseBox.xmin, baseBox.ymin], [baseBox.xmax, baseBox.ymin] , [baseBox.xmax, baseBox.ymax],[baseBox.xmin, baseBox.ymax]];
	else
		return [ [baseBox.xmin, baseBox.ymin], [baseBox.xmin, baseBox.ymax] , [baseBox.xmax, baseBox.ymax], [baseBox.xmax, baseBox.ymin]];
}

function GetMaxDimension(baseBox)
{  
	return ( baseBox.xmax-baseBox.xmin > baseBox.ymax-baseBox.ymin ) ? (baseBox.xmax-baseBox.xmin) : (baseBox.ymax-baseBox.ymin);
}

function GetExtremeIndicesOnXAxis(toolDefinition)
{
	var xmin, xmax, indexMin=0, indexMax=0;
	xmin = toolDefinition[0][0];
	xmax = toolDefinition[0][0];
	
	for(var i=0; i<toolDefinition.length; i++)
		{
		
		  if( toolDefinition[i][0] < xmin)
			  {
			  xmin = toolDefinition[i][0];
			  indexMin = i;
			  }
		  
		  if( toolDefinition[i][0] > xmax)
		      {
		      xmax = toolDefinition[i][0];
		      indexMax = i;
		      }
		}
	return [indexMin, indexMax];
}

function GetStraightLineToolPolygon(toolDefinition, toolAngle, maxDimension)
{	
	let minmaxIndices = GetExtremeIndicesOnXAxis(toolDefinition);
	let toolPathPolygon = [];
	// Create a polygon of extrme left and extreme right points and their projects to the y min.
	toolPathPolygon[0] = [toolDefinition[minmaxIndices[0]][0], toolDefinition[minmaxIndices[0]][1] + maxDimension];
	toolPathPolygon[1] = [toolDefinition[minmaxIndices[0]][0], toolDefinition[minmaxIndices[0]][1]];
	toolPathPolygon[2] = [toolDefinition[minmaxIndices[1]][0], toolDefinition[minmaxIndices[1]][1]];	
	toolPathPolygon[3] = [toolDefinition[minmaxIndices[1]][0], toolDefinition[minmaxIndices[1]][1] + maxDimension];

	// For each points of the tool definition, if if is outside the polygon, add it to extrapoints.
	var toolPathRectangle = GetPolygon(toolPathPolygon);
	let extraPoints = [];
	for(var j=0; j<toolDefinition.length; j++)
	{
		if(!toolPathRectangle.contains(point(toolDefinition[j][0], toolDefinition[j][1])))
		{
			 extraPoints.push([toolDefinition[j][0], toolDefinition[j][1]]);
		}
	}
	
	//All such extra points should be added to the toolPathPolygon in the middle
	// First 2 and the last 2 points are the same as above.
	var completeToolPathPolygon = [];
	completeToolPathPolygon[0] = toolPathPolygon[0];
	completeToolPathPolygon[1] = toolPathPolygon[1];
	for(var j=0; j<extraPoints.length; j++)
	{
		completeToolPathPolygon[j+2] = extraPoints[j];
	}
	
	completeToolPathPolygon[extraPoints.length + 2] = toolPathPolygon[2];
	completeToolPathPolygon[extraPoints.length + 3] = toolPathPolygon[3];
	
	let rotated = [];;
	let rotatedToolDefinition = [];
	for(var j=0; j<completeToolPathPolygon.length; j++)
	{
		rotated[j]= point(completeToolPathPolygon[j]).rotate(toolAngle); 
		rotatedToolDefinition[j] = [rotated[j].x, rotated[j].y];
	}

	var count = toolDefinition.length;  	
	return rotatedToolDefinition;
}


function GetCutPolygonForTwoPoints(arrOfTwoPoints, toolDefinition, baseBlockSubtracted, toolAngle, maxDimension )
{   
	//swap points in case reverse direction in order to maintain winding.
	if(arrOfTwoPoints[0][0] > arrOfTwoPoints[1][0])
	{
		var temp = arrOfTwoPoints[0];
		arrOfTwoPoints[0] = arrOfTwoPoints[1];
		arrOfTwoPoints[1] = temp;
	}
	
	// Rotate the tool by the specified angle
	let rotatedToolDefinition = RotateTool(toolDefinition,toolAngle);

	// Create a polygon of 2 path points and 2 points projected in -ive y direction by maxDimension
	let arrOfPointsAndProjections = [ ];
	arrOfPointsAndProjections[0] = [arrOfTwoPoints[0][0], arrOfTwoPoints[0][1] + maxDimension]; 
	arrOfPointsAndProjections[1] = arrOfTwoPoints[0];
	arrOfPointsAndProjections[2] = arrOfTwoPoints[1];
	arrOfPointsAndProjections[3] = [arrOfTwoPoints[1][0], arrOfTwoPoints[0][1] + maxDimension];
	//Apply rotation to the projected points, if needed
	let rotationPoints = [arrOfTwoPoints[0], arrOfTwoPoints[0], arrOfTwoPoints[1], arrOfTwoPoints[1]];
	
	let rotated = [];
	for(var j=0; j<4; j++)
	{		
		let x = (arrOfPointsAndProjections[j][0] -  rotationPoints[j][0]);
		let y = (arrOfPointsAndProjections[j][1] -  rotationPoints[j][1]);
	    let p = point(x,y);
		rotated[j]= p.rotate(toolAngle); 
		arrOfPointsAndProjections[j] = [rotated[j].x + rotationPoints[j][0], rotated[j].y + rotationPoints[j][1]]; 
	}
	      
	// Get a path for each point on the tool definition
	let newArrToolPath = [];
	for(var i=0; i<rotatedToolDefinition.length; i++)
	{
		let currentpath = [];
		for(var j=0; j<arrOfPointsAndProjections.length; j++)
		{
			currentpath.push([ arrOfPointsAndProjections[j][0] + rotatedToolDefinition[i][0], 
				            arrOfPointsAndProjections[j][1] + rotatedToolDefinition[i][1] ]);
		}
		newArrToolPath.push(currentpath);
	}

	let succeeded = true;
	let polygonsOfToolPoints = [];
	for (var r=0; r<toolDefinition.length; r++)
	{
		polygonsOfToolPoints[r] = GetPolygon(newArrToolPath[r]);
		
		if(polygonsOfToolPoints[r].area() < 0.00001)
		{
			succeeded = false;
			break;
		}
	}

	var finalPolygon = baseBlockSubtracted;
	// Straight line case: consider full polygon of the tool
	if(succeeded == false)
	{
		var rotatedToolDefinition2 = GetStraightLineToolPolygon(toolDefinition, toolAngle, maxDimension);

		for(var j=0; j<arrOfPointsAndProjections.length; j++)
		{
			let currentpath = [];

			for(var i=0; i<rotatedToolDefinition2.length; i++)
			{
				currentpath.push([ arrOfPointsAndProjections[j][0] + rotatedToolDefinition2[i][0], 
					            arrOfPointsAndProjections[j][1] + rotatedToolDefinition2[i][1] ]);
			}

			var polygonOnToolPath = GetPolygon(currentpath);
			//document.getElementById("stage").innerHTML += polygonOnToolPath.svg();
			var polygon = subtract(finalPolygon, polygonOnToolPath);
			
			if( polygon.vertices.length != 0 )
			{
					finalPolygon = polygon;
			}
		}
	}

	else 
	{
		var unifiedPolygon = GetUnifiedPolygons(polygonsOfToolPoints);
		finalPolygon = subtract(finalPolygon, unifiedPolygon)
	}
	
	return finalPolygon;
}

function ArrayFormatting(arrayToFormat)
{
	var FormattedArray = [];
	//var array =  [[12, 23], [23, 43], [56, 78], [100,235], [0, 0] , [10, 20], [20, 50]];
	var item = [0, 0];
	var isArrayInArrayIndex = 0;
	for(var j = 0; j < arrayToFormat.length; j++)
	{	
		if ( arrayToFormat[j][0] == 0 && arrayToFormat[j][1] == 0 ) 
		{
			isArrayInArrayIndex = j;
			break;
		}
	}
	for(var i = isArrayInArrayIndex; i>= 0; i--)
	{
		FormattedArray.push(arrayToFormat[i][1]);
		FormattedArray.push(arrayToFormat[i][0]);
		
	}
	for(var i = arrayToFormat.length-1 ; i > isArrayInArrayIndex; i--)
	{
		FormattedArray.push(arrayToFormat[i][1]);
		FormattedArray.push(arrayToFormat[i][0]);
	}
	return FormattedArray;
	//document.getElementById("stage").innerHTML += FormattedArray.svg();
}