import {point, circle, polygon, arc, segment, box} from "https://unpkg.com/@flatten-js/core?module";
import Flatten from 'https://unpkg.com/@flatten-js/core?module';
import cutPolygon from './cutPolygon.js';
import StandardOD_Tool from './StandardOD_Tool.js';
import mathUtils from './MathUtils.js';
let {unify, subtract} = Flatten.BooleanOperations;
let arrToolPath = [];
function main()
{
	var rhombusTool = new StandardOD_Tool( (Math.PI/4), Math.PI/20,  10);
	//let arrOfToolDefinition = rhombusTool.GetToolPoints();
	//let arrOfBase = box(100, 100, 1000, 300);
	let arrOfBase = [0, 0, 1000, 300];
		
	let arrOfToolDefinition = [ [-20, 0], [0, 0], [20, 0] ];
	//let arrOfToolDefinition = [ [-5, -10], [0, 0], [5, -10] ];
	//let arrOfToolDefinition = [ [-10, -10], [0, 0],  [-5, -4], [10, -10] ];
	//let arrOfToolDefinition = [ [20, 0], [0, 0], [-20, 0] ];
	
	let time = 0;
	for(var i=1; i<=5; i++)
	{
	var t0 = performance.now();
	TestInterpolation();
	//Test_Horizontal_Tool_Horizontal_Path(arrOfBase, arrOfToolDefinition);  
	//Test_Straight_Tool_Random_Path(arrOfBase, arrOfToolDefinition);
	//Test_Straight_Tool_Straight_Path(arrOfBase, arrOfToolDefinition);  
	//Test_Horizontal_Tool_Horizontal_Path_TopmostLeft(arrOfBase, arrOfToolDefinition) ; 
	//Test_Horizontal_Tool_Horizontal_Path_TopmostRight(arrOfBase, arrOfToolDefinition);  
	//Test_Horizontal_Tool_Horizontal_Path_BottomRight(arrOfBase, arrOfToolDefinition);  //Does not work
	//Test_Horizontal_Tool_Horizontal_Path_BottomLeft(arrOfBase, arrOfToolDefinition);   //Does not work
	//Test_Straight_Tool_Vertical_Path_TopmostLeft_PartialInsertion(arrOfBase, arrOfToolDefinition);  
	//Test_Straight_Tool_Vertical_Path_TopmostRight_PartialInsertion(arrOfBase, arrOfToolDefinition);  
	//Test_Straight_Tool_Vertical_Path_BottomRight_PartialInsertion(arrOfBase, arrOfToolDefinition);  //Does not work
	//Test_Straight_Tool_Vertical_Path_BottomLeft_PartialInsertion(arrOfBase, arrOfToolDefinition);  //Does not work
	//Test_Angled_Tool_AngledStraight_Path(arrOfBase, arrOfToolDefinition);  //Does not work
	//Test_Straight_Tool_RandomAndStraight_Path(arrOfBase, arrOfToolDefinition);  
	//Test_Straight_Tool_RandomAndStraight_Path_50Points(arrOfBase, arrOfToolDefinition);
	//Test_Straight_Tool_RandomAndStraight_Path_25Points(arrOfBase, arrOfToolDefinition);
	//InvalidToolPath(arrOfBase, arrOfToolDefinition);
	
	var t1 = performance.now();
	console.log("Time took " + (t1 - t0) + " milliseconds.")
	time += t1 - t0;
	}
	
	let average = time/5;
	console.log("Time average " + (average) + " milliseconds.")	
}
main();

function TestInterpolation()
{
	let vec1 = [0, 0];
	let vec2 = [10, 0];
	let utils = new mathUtils();
	let interpolatedArray = utils.InsertPoints(vec1, vec2, 10);
	
	
	vec1 = [10, 0];
	vec2 = [-10, 10];
	utils = new mathUtils();
	interpolatedArray = utils.InsertPoints(vec1, vec2, 10);
	
}

// Test case 1
function Test_Horizontal_Tool_Horizontal_Path(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = (Math.PI)/2;
	
	// Horizontal tool path
	let toolPath = [ [1000, 200], [600, 200], [1000, 200] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 2
function Test_Straight_Tool_Random_Path(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	var vectorPointsBase = [0,0, 140, 60/2]
	var vectorPointsTool = [[-1, 0], [0, 0], [1,0]];
	
	//toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,20,60,30,80,30,90,23,140,0,140]);
	var vectorPointsPath1 = [[0,30], [3,30]];
	var vectorPointsPath2 = [[3,30], [3,29]];
	var vectorPointsPath3 = [[3,29], [10,3]];
	var vectorPointsPath4 = [[10,3], [60,3]];
	var vectorPointsPath5 = [[60,3], [60,20]];
	var vectorPointsPath6 = [[60,20], [80,30]];
	var vectorPointsPath7 = [[80,30], [90,30]];
	var vectorPointsPath8 = [[90,30], [140,23]];
		
	
	// Random tool path
	let toolPath =[ [200, 100], [200, 120], [450, 120]];
	//let toolPath =[ [200, 100], [200, 120], [450, 120]];
	//let toolPath =[ [200, 280], [200, 200],[200, 300]];
	var cutPolygonObj = new cutPolygon(vectorPointsBase, vectorPointsTool, 0);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(vectorPointsPath1);
	var unifiedPolygons2 = cutPolygonObj.GetFinalCutPath(vectorPointsPath2);
	var unifiedPolygons3 = cutPolygonObj.GetFinalCutPath(vectorPointsPath3);
	var unifiedPolygons4 = cutPolygonObj.GetFinalCutPath(vectorPointsPath4);
	var unifiedPolygons5 = cutPolygonObj.GetFinalCutPath(vectorPointsPath5);
	var unifiedPolygons6 = cutPolygonObj.GetFinalCutPath(vectorPointsPath6);
	var unifiedPolygons7 = cutPolygonObj.GetFinalCutPath(vectorPointsPath7);
	var unifiedPolygons8 = cutPolygonObj.GetFinalCutPath(vectorPointsPath8);
	
	document.getElementById("stage").innerHTML += unifiedPolygons8.svg(); 	
}
 
//Test case 3
function Test_Straight_Tool_Straight_Path(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Vertical tool path
	let toolPath = [ [300, 100], [300, 200], [300, 180], [300, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 4
function Test_Horizontal_Tool_Horizontal_Path_TopmostLeft(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = (3*Math.PI)/2;
	
	// Horizontal tool path from topmost left corner
	let toolPath = [ [100, 100], [200, 100], [100, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 5
function Test_Horizontal_Tool_Horizontal_Path_TopmostRight(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = (Math.PI)/2;
	
	// Horizontal tool path from topmost right corner
	let toolPath = [ [1000, 100], [800, 100], [1000, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 6
function Test_Horizontal_Tool_Horizontal_Path_BottomRight(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = (Math.PI)/2;
	
	// Horizontal tool path from bottom right corner
	let toolPath = [ [1000, 300], [800, 300], [1000, 300] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 7
function Test_Horizontal_Tool_Horizontal_Path_BottomLeft(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = (3*Math.PI)/2;
	
	// Horizontal tool path from bottom left corner
	let toolPath = [ [100, 300], [280, 300], [100, 300] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 8
function Test_Straight_Tool_Vertical_Path_TopmostLeft_PartialInsertion(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Vertical tool path from topmost left corner
	let toolPath = [ [-10, 100], [100, 150], [100, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 9
function Test_Straight_Tool_Vertical_Path_TopmostRight_PartialInsertion(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Vertical tool path from topmost right corner
	let toolPath = [ [1000, 100], [1000, 180], [1000, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 10
function Test_Straight_Tool_Vertical_Path_BottomRight_PartialInsertion(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = Math.PI;
	
	// Vertical tool path from bottom right corner
	let toolPath = [ [1000, 300], [1000, 150], [1000, 300] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 11
function Test_Straight_Tool_Vertical_Path_BottomLeft_PartialInsertion(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;//Math.PI;
	
	// Vertical tool path from bottom left corner
	let toolPath = [ [100, 300], [100, 150], [100, 300] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 12
function Test_Angled_Tool_AngledStraight_Path(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = Math.PI/4;
	
	// Straight and angled tool path
	let toolPath = [ [900, 200], [700, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg();
	
}

//Test case 13
function Test_Straight_Tool_RandomAndStraight_Path(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Straight and random tool path
	let toolPath = [ [200, 100], [200, 130], [250, 130], [250, 200], [250, 130], [300, 130], [300, 200], [300, 130], [400, 130], [400, 100] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 14
function Test_Straight_Tool_RandomAndStraight_Path_50Points(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Straight and random tool path for 50 points
	let toolPath = [ [100, 100], [120, 112], [140, 116], [160, 120], [180, 123], [200, 128], [220, 133], [240, 137], [260, 141], [280, 145], 
		             [300, 149], [320, 153], [340, 158], [360, 162], [380, 168], [400, 170], [420, 173], [440, 178], [460, 180], [480, 185], 
		             [500, 190], [520, 195], [540, 200], [560, 205], [580, 205], [600, 190], [620, 180], [640, 170], [660, 150], [680, 120],
		             [700, 145], [720, 145], [740, 145], [740, 130], [740, 120], [740, 110], [760, 150], [840, 150], [860, 160], [880, 170], 
		             [900, 180], [920, 200], [940, 220], [950, 220], [960, 220], [965, 220], [975, 220], [980, 220], [990, 220], [995, 220] ];
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 15
function Test_Straight_Tool_RandomAndStraight_Path_25Points(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Straight and random tool path for 25 points
	let toolPath = [ [110, 120], [125, 120], [140, 120], [140, 130], [140, 135],
		             [160, 135], [183, 135], [183, 140], [183, 150], [200, 150], 
		             [220, 150], [220, 160], [220, 165], [250, 165], [258, 165], 
		             [258, 172], [258, 180], [300, 180], [320, 180], [320, 190], 
		             [320, 200], [500, 200], [600, 150], [650, 150], [650, 100] ];
	
	//ReadCSV("toolPath.csv");
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

//Test case 16
function InvalidToolPath(arrOfBase, arrOfToolDefinition)
{
	let toolAngle = 0;
	
	// Invalid Tool Path
	let toolPath = [ [100, 100], [300, 130], [400, 150], [200, 120], [350, 100] ];
	
	//ReadCSV("toolPath.csv");
	
	var cutPolygonObj = new cutPolygon(arrOfBase, arrOfToolDefinition, toolAngle);   
		
	var unifiedPolygons = cutPolygonObj.GetFinalCutPath(toolPath);
	
	//document.getElementById("stage").innerHTML += unifiedPolygons.svg(); 	
}

