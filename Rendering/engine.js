import * as THREE from '../../three/build/three.module.js';

import {
	  Camera,
	  Material,
	  Texture,
	  AxesHelper
	} from '../../three/build/three.module.js';
	
import { OrbitControls } from 'https://unpkg.com/three@0.116.0/examples/jsm/controls/OrbitControls.js'

import {
	  BoxBufferGeometry,
	  Color,
	  Mesh,
	  MeshBasicMaterial,
	  PerspectiveCamera,
	  Scene,
	  WebGLRenderer,
	  Raycaster,
	  Vector2,
	} from '../../three/build/three.module.js';
import {TrackballControls} from 'https://unpkg.com/three@0.116.0/examples/jsm/controls/TrackballControls.js'
import selectInfo from './selectInfo.js';
import { Line2 } from '../../three/examples/jsm/lines/Line2.js';
import { LineMaterial } from '../../three/examples/jsm/lines/LineMaterial.js';
import { LineGeometry } from '../../three/examples/jsm/lines/LineGeometry.js';
import { GeometryUtils } from '../../three/examples/jsm//utils/GeometryUtils.js';
export default class Engine 
{
	constructor() 
	{
		this.windowInnerHeight = 0;
		this.canvasWidth = 0;
		this.canvasHeight = 0;
		this.previousSelectedX = 0;
		this.previousSelectedY = 0;
		this.entitiesForMeasurement = [];
		this.selectionInfo = new selectInfo;
		this.guiData = 0;
		this.distanceController = 0;
		this.scene = 0;
		this.resetController = 0;
		this.latheLength = 10;
		this.latheDiameter = 5;
		this.pathPoints = [];
		this.animationArrayIndex = 0;
		this.delayArray = [];
		this.previousRenderTimeStamp = 0;
		this.pauseFlag = false; 
		this.stopFlag = false;
		this.arrayPointsForTool = [];
		this.finalPathArray = [];
		this.latheTool = "";
		this.stock = "";
		this.baseObject = new THREE.Object3D();
	}
	
	Init(guiData, distanceController, resetController)
	{
		
		this.guiData = guiData;
		this.distanceController = distanceController;
		this.resetController = resetController;
		
		const canvas = document.querySelector('#c');
		this.renderer = new THREE.WebGLRenderer({canvas});		
		this.windowInnerHeight = window.innerHeight*0.9;
		this.canvasWidth = canvas.width;
		this.canvasHeight  = canvas.height;
		this.camera = new THREE.PerspectiveCamera(75, canvas.width / canvas.height, 0.1, 1000);
		this.camera.position.z = 10;
		this.raycaster = new THREE.Raycaster();
		this.mouse = new THREE.Vector2();

		// Adding controls
		this.controls = new TrackballControls(this.camera, this.renderer.domElement);
		//	  this.controls = new OrbitControls( this.camera, this.renderer.domElement );
	    this.controls.rotateSpeed = 0.5;
	    this.controls.panSpeed = 0.5;
	    this.controls.zoomSpeed = 0.5;
		this.scene = new THREE.Scene();
	    const color = 0xFFFFFF;
	    const intensity = 1;
	    const light = new THREE.DirectionalLight(color, intensity);
	    const ambientLight = new THREE.AmbientLight(0xffffff, 1); 
	    light.position.set(-1, 2, 4); 
	    this.scene.add(light);
	    this.scene.add(ambientLight);
	    
	    canvas.addEventListener('mousedown', this.OnDocumentMouseDown.bind(this), false);
	    
	    
	    const radiusTop = 5;
	    const radiusBottom = 5;
	    const height = 10;
	    const radialSegments = 32;
	    const material = new THREE.MeshStandardMaterial
	    ({
	  	    color: 0x888888,
	    		roughness:0.6,
	    		metalness:0.6
	    });

	    var axisHelper = new THREE.AxesHelper( 100);
	    this.scene.add(axisHelper);
		var degree = Math.PI/180;
		this.baseObject.rotation.set(-15*degree, 0, 35*degree);
		this.scene.add(this.baseObject);
		
	}

	 ResizeRendererToDisplaySize(rendererParam)
	 {
	    const canvas = rendererParam.domElement;
	    const pixelRatio = window.devicePixelRatio;
	    const width  = canvas.clientWidth  * pixelRatio | 0;
	    const height = canvas.clientHeight * pixelRatio | 0;
	    const needResize = canvas.width !== width || canvas.height !== height;
	    this.canvasWidth = canvas.clientWidth;
	    this.canvasHeight = canvas.clientHeight;
	    if (needResize) 
	    {
	    	rendererParam.setSize(width, height, false);
	    }
	    
	    return needResize;
	  }


	// Run game loop (render,repeat)
	Render()
	{
		if (this.ResizeRendererToDisplaySize(this.renderer)) 
		{
			const canvas = this.renderer.domElement;
			this.camera.aspect = canvas.clientWidth / canvas.clientHeight;
			this.camera.updateProjectionMatrix();
		}
		
		this.renderer.render(this.scene, this.camera);	
		let selectionMode = this.guiData.Measure;
		
		if(selectionMode)
		{
		}
		else 
		{
			this.controls.update();
		}
	}
	
	
	 OnDocumentMouseDown(event) 
	 {		
		 
		 var objleftNBDIV = document.getElementById("CanvasDiv");
		 var elPos= this.getElementPos(objleftNBDIV);
		 var Canvas_X_Y= document.getElementById("c");
		 var elPosCanvas_X_Y= this.getElementPos(Canvas_X_Y);
		 var tempOffset = this.canvasWidth-elPos.lx;
		 var CanvasOffset = elPosCanvas_X_Y.lx - elPos.lx
		 
		 var vector = new THREE.Vector3(( (event.clientX - elPos.lx) / (this.canvasWidth) ) * 2 - 1, -((event.clientY - elPos.ly) / this.canvasHeight ) * 2 + 1, 0.5);
	        vector = vector.unproject(this.camera);

	        var raycaster = new THREE.Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());
	        var intersects = raycaster.intersectObjects(this.entitiesForMeasurement);
	        let selectionMode = this.guiData.Measure;
	       if(intersects.length >0 && (selectionMode))
	        {	
	        	this.selectionInfo.AddPoint(intersects[0].point,this.scene);
	        	this.distanceController.object.Distance = (this.selectionInfo.distance).toFixed(2);
	        	this.distanceController.updateDisplay();
	        	
	        }
	        else if(intersects.length ==0)
	        {
	        	//this.selectionInfo = "";
	        	this.scene.remove(this.selectionInfo.selectionInfo);
	        	this.Render();
	        	//this.selectionInfo = new selectInfo;
	        	//this.distanceController.object.Measure = "";
	        	//this.distanceController.updateDisplay();
	        }	
	        //document.getElementById("IdDistance").innerHTML=" Distance: "+(this.selectionInfo.distance).toFixed(2);
     }
	 
	  getElementPos(el) {
		 	for (var lx=0, ly=0;
			         el != null;
			         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
			   //SS:22082019  return {x: lx,y: ly};
		 	 return {lx,ly};
			}
	  
	  
	AdjestCamera()
	{
		var degree = Math.PI/180;
		this.camera.position.set(0, this.latheLength* 1.8, 1);
		this.camera.up = new THREE.Vector3(0,0,1);
	}
	
	
	async UpdateToolPath(arrayPathPoints, toolPosition, delay)
	{
		//await sleep(delay);
		var arrayOf2DPointes = [];
		arrayOf2DPointes = listToMatrix(arrayPathPoints, 2);
		this.PopulateLatheProfile(arrayOf2DPointes);
		this.PopulateToolPathProfile(arrayOf2DPointes);
		this.moveToLocation(toolPosition);
		this.renderer.render(this.scene, this.camera);	
	}
	
	
	InitializeLatheParams( latheLength, latheDiameter)
	{
		 this.latheLength = latheLength;
		 this.latheDiameter = latheDiameter;
		 var vectorPoints = [[0, 0], [0, (this.latheLength/2)], [this.latheDiameter, (this.latheLength/2)], [this.latheDiameter, 0]];
		 //this.PopulateLatheProfile(vectorPoints);
	}
	
	
	PopulateToolProfile(arrayOfToolPoints)
	{
		 var makeVertices = points =>
			new Array(points.length).fill(null).map((d, i) => new THREE.Vector2(points[i][0], points[i][1]));
			var VectorPoints = makeVertices(arrayOfToolPoints);
		
		const shape = new THREE.Shape();

		shape.moveTo(arrayOfToolPoints[0][0],arrayOfToolPoints[0][1]);
		for(var i = 1; i < arrayOfToolPoints.length; i++)
		{
			shape.lineTo(arrayOfToolPoints[i][0],arrayOfToolPoints[i][1]);
		}	
		
		const extrudeSettings = {
				  steps: 2,  
				  depth: 2,  
				  bevelEnabled: false
				};
		const geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
	 
		//const latheGeometry = new THREE.LatheGeometry(VectorPoints, 50);
		 const toolMaterial = new THREE.MeshStandardMaterial
		    ({
		  	    color: 0xff6666,
		    		roughness:0.7,
		    		metalness:0.6,
		    		side: THREE.DoubleSide
		    });
		this.latheTool = new THREE.Mesh(geometry, toolMaterial);
		// Rotate the entity to an isometric view.
		var degree = Math.PI/180;
		this.latheTool.name = "toolLathe"
		this.baseObject.add(this.latheTool);
		var degree = Math.PI/180;
		this.latheTool.rotation.set(-90*degree, -90*degree, 0);
		this.latheTool.position.set(0,0,-5);
	}
	
	
	
	PopulateLatheProfile(arrayOf2DPointes)
	{
		this.entitiesForMeasurement = [];
		if(this.stock != "" && this.stock != undefined)
		{
			this.baseObject.remove(this.stock);
		}

		var makeVertices = points =>
		new Array(points.length).fill(null).map((d, i) => new THREE.Vector2(points[i][0], points[i][1]));
		var VectorPoints = makeVertices(arrayOf2DPointes);
		const latheGeometry = new THREE.LatheGeometry(VectorPoints, 50);
		const latheMaterial = new THREE.MeshStandardMaterial
		({
			color: 0xbfbfbf,
			roughness:0.7,
			metalness:0.6,
			side: THREE.DoubleSide
		});
		this.stock = new THREE.Mesh(latheGeometry, latheMaterial);
		this.entitiesForMeasurement.push(this.stock);
		
		this.baseObject.add(this.stock);
		this.AdjestCamera();
	}
	 
	 ResetView()
	 {
		 this.animationArrayIndex = 0;
		 this.controls.reset();
		 //this.UpdateToolPath(this.animationArrayIndex);
	 }
	 
	 PopulateToolPathProfile(arrayOf2DPointes)
	 {
		 const linePositions = [];
			for(var i = 0; i<arrayOf2DPointes.length; i++)
			{
				linePositions.push(arrayOf2DPointes[i][0],arrayOf2DPointes[i][1],0);

			}	
			const colors = [];

			const lineGeometry = new LineGeometry();
			lineGeometry.setPositions( linePositions );
			var matLine = new LineMaterial( {
				color: 0xf58a42,
				linewidth: 0.005,
				scale: 1,
				dashSize: 3,
				gapSize: 1,

			} );
			let selectedObjectForLine = this.scene.getObjectByName("line");
			if(selectedObjectForLine != "" && selectedObjectForLine != undefined)
			{
				this.baseObject.remove(selectedObjectForLine);
			}
			var degree = Math.PI/180;
			var line = new Line2( lineGeometry, matLine );
			line.computeLineDistances();
			line.rotation.set(0, -90*degree, 0);
			line.name = "line";
			this.baseObject.add( line );
	 }
	 
	moveToLocation(location)
	{
		if(this.latheTool != "" )
		{
			this.latheTool.position.set(1,location[1], location[0]);
		}
		
	}
	
};

function sleep(ms) {
	  return new Promise(resolve => setTimeout(resolve, ms || 1000));
}
