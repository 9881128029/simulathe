import * as THREE from '../../three/build/three.module.js';

export default class selectInfo
{

	constructor()
	{
		this.selectedPoints = [2];
		this.selectedIndex = 0;
		this.distance = 0;
		this.selectionInfo = new THREE.Object3D();
	}

	AddPoint(point,scene)
	{

		this.selectedPoints[this.selectedIndex] = point;
		const radius = 0.1;  
		const widthSegments = 0.1;  
		const heightSegments = 0.1;  
		const material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
		const sphereGeometry = new THREE.SphereGeometry(radius, widthSegments, heightSegments);
		
		this.selectedIndex++;

		if(this.selectedIndex == 1)
		{
			this.distance = 0;
			scene.remove(this.selectionInfo);
			this.selectionInfo = new THREE.Object3D();
			CreateSphere(sphereGeometry,material,this.selectedPoints[0],this.selectionInfo);
			scene.add(this.selectionInfo);
		}
		else if(this.selectedIndex == 2)
		{

			var v1 = new THREE.Vector3(this.selectedPoints[0].x, this.selectedPoints[0].y, this.selectedPoints[0].z);
			var v2 = new THREE.Vector3(this.selectedPoints[1].x, this.selectedPoints[1].y, this.selectedPoints[1].z);
			this.distance = v1.distanceTo(v2);
			this.selectedIndex = 0;
			CreateSphere(sphereGeometry,material,this.selectedPoints[1], this.selectionInfo);
			var LineGeometry = new THREE.Geometry();
			LineGeometry.vertices.push(new THREE.Vector3(this.selectedPoints[0].x, this.selectedPoints[0].y, this.selectedPoints[0].z)); //x, y, z
			LineGeometry.vertices.push(new THREE.Vector3(this.selectedPoints[1].x, this.selectedPoints[1].y, this.selectedPoints[1].z));
			/* linewidth on windows will always be 1 */
			//var LineMaterial = new THREE.LineBasicMaterial( { color: '#f58a42', linewidth: 1 } );
			var LineMaterial = new THREE.LineDashedMaterial( {
				color: 0xf58a42,
				linewidth: 2,
				scale: 1,
				dashSize: 3,
				gapSize: 1,
			} );
			var line = new THREE.Line(LineGeometry, LineMaterial);
			//line.frustumCulled = false;
			line.computeLineDistances();
			this.selectionInfo.add(line);

		}


	}
	
}

function CreateSphere(sphereGeometry,material,selectedPoint, selectionInfo)
{
	const sphere1 = new THREE.Mesh( sphereGeometry, material );
	selectionInfo.add(sphere1);
	sphere1.position.x = selectedPoint.x;
	sphere1.position.y = selectedPoint.y;
	sphere1.position.z = selectedPoint.z;
}