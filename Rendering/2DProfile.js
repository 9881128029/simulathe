var toolPathDataFor2D = [];
function transformPoints(data, dataWidth, dataHeight, areaWidth, areaHeight)
{	
	var  transformedDataLower = [];
	var  transformedDataUpper = [];
	const xRatio = 0.9 * areaWidth/dataWidth;
	const offset = 0.5 * (areaWidth - xRatio * dataWidth);
	for(var i = 0; i < data.length; i++)
	{
		 var Matrix = [];
		 Matrix = listToMatrix(data, 2);
		 var tempTransformedDataLower = [];
		 for(var j = 0; j < Matrix.length; j++)
		 {
			 let x = parseFloat(Matrix[j][1]);
			 let y = parseFloat(Matrix[j][0]);
			 tempTransformedDataLower[j] = [ xRatio*x + offset, 
				 xRatio*y + parseFloat(areaHeight/2 - dataHeight/2)];
		 }	 
		 if(tempTransformedDataLower.length > 0)
		 {
			 transformedDataLower[i] = tempTransformedDataLower;
		 }	 
	}
	
	for(var i = 0; i < data.length; i++)
	{
		 var Matrix = [];
		 Matrix = listToMatrix(data, 2);
		 var tempTransformedDataUpper = [];
		 for(var j = 0; j < Matrix.length; j++)
		 {
			 let x = parseFloat(Matrix[j][1]);
			 let y = -parseFloat(Matrix[j][0]);
			 tempTransformedDataUpper[j] = [ (xRatio*x +offset/*+ parseFloat(areaWidth/2 - dataWidth/2)*/), 
				 (xRatio*y + parseFloat(areaHeight/2 - dataHeight/2))];
		 }	 
		 if(tempTransformedDataUpper.length > 0)
		 {
			 transformedDataUpper[i] = tempTransformedDataUpper;
		 }	
	}
	//toolPathDataFor2D.push(transformedDataLower);
	//toolPathDataFor2D.push(transformedDataUpper);
	return [transformedDataLower,transformedDataUpper];
}