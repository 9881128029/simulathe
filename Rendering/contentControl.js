import Engine from './engine.js';
import AnimaterFor2D from './2D_animater.js';
import CommandProcessor from './commandProcessor.js'; 
import cutPolygon from '../../Code/Maths/cutPolygon.js';
import StandardOD_Tool from '../../Code/Maths/StandardOD_Tool.js';

var guiData = function() {
	this.Distance = "";
	this.Measure = false;
	this.Reset = function() 
	{
		GlobalEngine.ResetView();
	}
	/*this.Play = function() 
	{
		var delayArray = [];
		for(var i = 0; i < 11; i++)
		{
			delayArray.push(500);
		}	
		GlobalEngine.Animate(toolPathCsvData, delayArray , this.toolPathDataFor2D);
		//GlobalEngineAnimate2DProfile(toolPathDataFor2D, delayArray);
	}
	this.Pause = function() 
	{
		GlobalEngine.PauseAnimation();
	}
	this.Stop = function() 
	{
		GlobalEngine.StopAnimation();
	}*/
	
	this.points = [];
	this.toolPathDataFor2D = [];
};

var GlobalEngine = 0;
var flagPauseClicked = false;
var AninmaterFor2DProfile = 0;
var commandProcessor = 0;
var indexOfPlayAnimation = 0;
export default class contentControl
{
	constructor()
	{
		this.gui = new dat.GUI();
		this.GuiData = new guiData();
	}
	Init()
	{
		this.AddGUIdata();
		this.engine =  new Engine();
		this.commandProcessor = new CommandProcessor(this.engine);
		this.engine.Init(this.GuiData, this.distanceController,this.resetController);
		GlobalEngine = this.engine;
		AninmaterFor2DProfile = new AnimaterFor2D();
		this.engine.InitializeLatheParams(140, 60);
	}	
	AddGUIdata()
	{
		this.gui.add(this.GuiData, 'Measure');
		this.distanceController = this.gui.add(this.GuiData, 'Distance');
		this.resetController = this.gui.add(this.GuiData, 'Reset');
		
		var resetControllerStyle = this.resetController.domElement.previousSibling.style;
		resetControllerStyle.backgroundImage = 'url(images/reset.png)';
		resetControllerStyle.backgroundRepeat = 'no-repeat';
		resetControllerStyle.backgroundPosition = 'center';
		//this.gui.add(this.GuiData, 'Animate')
		
	}
	
	PopulateCSVData(toolPathCsvData)
	{
		this.engine.InitializeLatheParams(140, 40);
	}

	async PlayAnimation()
	{
		flagPauseClicked = false;
		indexOfPlayAnimation = 0;
		var ArrayOfVectorPoints = [];
		
		ArrayOfVectorPoints.push([0,30]); 
		ArrayOfVectorPoints.push([3,30]);
		ArrayOfVectorPoints.push([3,30]); 
		ArrayOfVectorPoints.push([3,29]);
		ArrayOfVectorPoints.push([3,29]); 
		ArrayOfVectorPoints.push([10,3]);
		ArrayOfVectorPoints.push([10,3]); 
		ArrayOfVectorPoints.push([60,3]);
		ArrayOfVectorPoints.push([60,3]); 
		ArrayOfVectorPoints.push([60,20]);
		ArrayOfVectorPoints.push([60,20]); 
		ArrayOfVectorPoints.push([80,30]);
		ArrayOfVectorPoints.push([80,30]); 
		ArrayOfVectorPoints.push([90,30]);
		ArrayOfVectorPoints.push([90,30]); 
		ArrayOfVectorPoints.push([140,23]);
		
		/*ArrayOfVectorPoints.push([[0,30], [3,30]]);
		ArrayOfVectorPoints.push([[3,30], [3,29]]);
		ArrayOfVectorPoints.push([[3,29], [10,3]]);
		ArrayOfVectorPoints.push([[10,3], [60,3]]);
		ArrayOfVectorPoints.push([[60,3], [60,20]]);
		ArrayOfVectorPoints.push([[60,20], [80,30]]);
		ArrayOfVectorPoints.push([[80,30], [90,30]]);
		ArrayOfVectorPoints.push([[90,30], [140,23]]);*/
		/*var vectorPointsPath1 = [[0,30], [3,30]];
		var vectorPointsPath2 = [[3,30], [3,29]];
		var vectorPointsPath3 = [[3,29], [10,3]];
		var vectorPointsPath4 = [[10,3], [60,3]];
		var vectorPointsPath5 = [[60,3], [60,20]];
		var vectorPointsPath6 = [[60,20], [80,30]];
		var vectorPointsPath7 = [[80,30], [90,30]];
		var vectorPointsPath8 = [[90,30], [140,23]];*/
		/*var height = document.getElementById('svgArea').clientHeight;
		var width = document.getElementById('svgArea').clientWidth;
		var dataWidth = 28;
		var dataHeight = 141;*/
		if(indexOfPlayAnimation >= ArrayOfVectorPoints.length )
		{
			this.InitializeCommandProcessor();
			indexOfPlayAnimation = 0;
		}
		if(indexOfPlayAnimation == 0)
		{
			this.InitializeCommandProcessor();
		}	
		/*if(flagPauseClicked == true)
		{
			indexOfPlayAnimation = indexOfPlayAnimation;
			flagPauseClicked = false
		}*/
		for(var i = indexOfPlayAnimation; i < ArrayOfVectorPoints.length; i++)
		{
			if(flagPauseClicked == true)
			{
				indexOfPlayAnimation = indexOfPlayAnimation;
				//flagPauseClicked = false
				break;
			}
			//var delay = ArrayOfVectorPoints[i][1]*50;
			
			//AninmaterFor2DProfile.Animate(this.toolPathDataFor2D, delayArray);
			
			await this.commandProcessor.MoveToPosition(ArrayOfVectorPoints[i], 0);
			/*var transformedData = transformPoints(ArrayOfVectorPoints[i], dataHeight,dataWidth, width, height);
			var transformedDataFor2D_Upper = [];
			var transformedDataFor2D_Lower = [];
			for(var j = 0; j < transformedData[0].length; j++)
			{
				transformedDataFor2D_Upper.push(transformedData[0][j]);
				transformedDataFor2D_Lower.push(transformedData[1][j]);
			}	
			
			await AninmaterFor2DProfile.Update2DToolPath(transformedDataFor2D_Upper, transformedDataFor2D_Lower, delay, indexOfPlayAnimation);*/
			indexOfPlayAnimation++;
		}
		
		var delayArray = [];
		for(var i = 0; i < 20; i++)
		{
			delayArray.push(500);
		}	
		/*
		var vectorPointsBase = [0,0, 140, 60/2]
		var vectorPointsTool = [[-1, 0], [0, 0], [1,0]];
		
		//Tool path points
		var vectorPointsPath1 = [[0,30], [3,30]];
		var vectorPointsPath2 = [[3,30], [3,29]];
		var vectorPointsPath3 = [[3,29], [10,3]];
		var vectorPointsPath4 = [[10,3], [60,3]];
		var vectorPointsPath5 = [[60,3], [60,20]];
		var vectorPointsPath6 = [[60,20], [80,30]];
		var vectorPointsPath7 = [[80,30], [90,30]];
		var vectorPointsPath8 = [[90,30], [140,23]];
		
		var cutPolygonObj = new cutPolygon(vectorPointsBase, vectorPointsTool, 0);   
		var unifiedPolygons = cutPolygonObj.GetFinalCutPath(vectorPointsPath1);
		var unifiedPolygons2 = cutPolygonObj.GetFinalCutPath(vectorPointsPath2);
		var unifiedPolygons3 = cutPolygonObj.GetFinalCutPath(vectorPointsPath3);
		var unifiedPolygons4 = cutPolygonObj.GetFinalCutPath(vectorPointsPath4);
		var unifiedPolygons5 = cutPolygonObj.GetFinalCutPath(vectorPointsPath5);
		var unifiedPolygons6 = cutPolygonObj.GetFinalCutPath(vectorPointsPath6);
		var unifiedPolygons7 = cutPolygonObj.GetFinalCutPath(vectorPointsPath7);
		var unifiedPolygons8 = cutPolygonObj.GetFinalCutPath(vectorPointsPath8);
		
		var FinalCutPathArrayOfPoints = [];
		FinalCutPathArrayOfPoints.push(unifiedPolygons);
		FinalCutPathArrayOfPoints.push(unifiedPolygons2);
		FinalCutPathArrayOfPoints.push(unifiedPolygons3);
		FinalCutPathArrayOfPoints.push(unifiedPolygons4);
		FinalCutPathArrayOfPoints.push(unifiedPolygons5);
		FinalCutPathArrayOfPoints.push(unifiedPolygons6);
		FinalCutPathArrayOfPoints.push(unifiedPolygons7);
		FinalCutPathArrayOfPoints.push(unifiedPolygons8);*/
		
		//GlobalEngine.Animate(FinalCutPathArrayOfPoints, delayArray);
		//GlobalEngine.UpdateToolPath(unifiedPolygons, toolPosition);
		//GlobalEngine.Animate(toolPathCsvData, delayArray);
		//AninmaterFor2DProfile.Animate(this.toolPathDataFor2D, delayArray);
	}
	PauseAnimation() 
	{
		//GlobalEngine.PauseAnimation();
		//AninmaterFor2DProfile.PauseAnimation();
		//indexOfPlayAnimation = 0;
		flagPauseClicked = true;
	}
	StopAnimation() 
	{
		GlobalEngine.StopAnimation();
		AninmaterFor2DProfile.StopAnimation();
	}
	
	AnimationFor2D(toolPathDataFor2D) 
	{
		this.toolPathDataFor2D = [];
		this.toolPathDataFor2D = toolPathDataFor2D;
		/*var delayArray = [];
		for(var i = 0; i < 11; i++)
		{
			delayArray.push(500);
		}	*/
		//GlobalEngine.Animate2DProfile(toolPathDataFor2D, delayArray);
	}
	
	InitializeCommandProcessor()
	{
		var vectorPointsTool = [[-1, 0], [0, 0], [1,0]];
		
		var IC = 3;
		var A = (Math.PI/4);
		var BA = Math.PI/10;
		var obj = new StandardOD_Tool(A, BA, IC);
		var tool = obj.GetToolPoints();
		
		
		this.commandProcessor.DefineStock(60, 140);
		this.commandProcessor.DefineTool("", tool);
		this.commandProcessor.InitializeMathsModule();
	}
	
}

