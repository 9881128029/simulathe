var vectorPointsForLower2DProfile = [];
var vectorPointsForUpper2DProfile = [];
export default class AnimaterFor2D 
{
	constructor() 
	{
		this.delayArray = [];
		this.previousRenderTimeStamp = 0;
		this.pauseFlag = false; 
		this.stopFlag = false;
		this.pathPointsForLower2DProfile = [];
		this.pathPointsForUpper2DProfile = [];
		this.pointToBeAddedForLower2DProfile = [];
		this.pointToBeAddedForUpper2DProfile = [];
	}
	Render2DProfile(time)
	{
		var timeNow = Date.now();
		var elapsed = timeNow - this.previousRenderTimeStamp;
		var delay = 0;
		if(this.delayArray.length > 0 &&  this.counter != this.delayArray.length)
		{
			delay = this.delayArray[this.counter];
		}	
		if(elapsed > delay)
		{
			time *= 0.001;
			
			if(this.pathPoints.length > 0 && this.pauseFlag == false)
			{	
				//this.UpdateToolPath(this.counter);
				//this.Update2DToolPath(this.counter);
			}
			
			this.previousRenderTimeStamp = timeNow;
		}
		
		requestAnimationFrame(this.Render2DProfile.bind(this));	
	}
	
	PauseAnimation()
	{
		if(this.counter != 0)
			this.pauseFlag = true;
	}
	
	StopAnimation()
	{
		if(this.counter != 0)
		{	
			this.stopFlag = true;
			this.pauseFlag = false;
		}
	} 
	Animate(arrayOf2Dpoints, delayArray)
	{
		if(this.pauseFlag == false)
		{
			this.counter = 0;
			this.pointToBeAdded = [];
			this.pathPoints = arrayOf2Dpoints;
			this.delayArray = delayArray;
			this.stopFlag = false;
			this.pointToBeAddedForLower2DProfile = [];
			this.pointToBeAddedForUpper2DProfile = [];
			this.pathPointsForLower2DProfile = arrayOf2Dpoints[0];
			this.pathPointsForUpper2DProfile = arrayOf2Dpoints[1];
		}
		else
		{
			this.pauseFlag = false;
		}
		//this.Render2DProfile();
		requestAnimationFrame(this.Render2DProfile.bind(this));
	}
	
	/*Update2DToolPath(counter)
	{
		
		if(counter < this.pathPointsForLower2DProfile.length )
		{	
			var vectorPointsForLower2DProfile = [];
			var vectorPointsForUpper2DProfile = [];
			this.pointToBeAddedForLower2DProfile = [];
			this.pointToBeAddedForUpper2DProfile = [];
			if(this.pathPointsForLower2DProfile.length > 0)
			{	
				
				this.pointToBeAddedForLower2DProfile.push([(this.pathPointsForLower2DProfile[counter])])//, parseInt(this.pathPointsForLower2DProfile[counter][1])]);
				
				for(var i = 0; i < this.pointToBeAddedForLower2DProfile.length ; i++ )
				{
					vectorPointsForLower2DProfile.push(this.pointToBeAddedForLower2DProfile[i]);
				}	
				document.getElementById("ShapeOnRectLower").setAttribute("points", vectorPointsForLower2DProfile);
				this.pointToBeAddedForUpper2DProfile.push([(this.pathPointsForUpper2DProfile[counter])])//, parseInt(this.pathPointsForUpper2DProfile[counter][1])]);
				
				for(var i = 0; i < this.pointToBeAddedForUpper2DProfile.length ; i++ )
				{
					vectorPointsForUpper2DProfile.push(this.pointToBeAddedForUpper2DProfile[i]);
				}
				
				document.getElementById("ShapeOnRectUpper").setAttribute("points", vectorPointsForUpper2DProfile);
				//this.PopulateLatheProfile(vectorPoints);
				this.counter++;
			}
			else
			{
				document.getElementById("ShapeOnRectLower").setAttribute("points", vectorPointsForLower2DProfile);
				document.getElementById("ShapeOnRectUpper").setAttribute("points", vectorPointsForUpper2DProfile);
			}
		}
		if(this.stopFlag == true)
		{
			this.counter = 0;
			this.delayArray = [];
			this.pathPointsForLower2DProfile = [];
			this.pathPointsForUpper2DProfile = [];
			this.pointToBeAddedForLower2DProfile = [];
			this.pointToBeAddedForUpper2DProfile = [];
			this.pauseFlag = false;
			document.getElementById("ShapeOnRectLower").setAttribute("points", "");
			document.getElementById("ShapeOnRectUpper").setAttribute("points", "");
		}	
		
	}*/
	
	async Update2DToolPath(upperToolPathArray, lowerToolPath, delay)
	{
		await sleep(delay);
		/*if(counter < this.pathPointsForLower2DProfile.length )
		{*/	
			/*if(indexOfPlayAnimation == 0)
			{*/
				vectorPointsForLower2DProfile = [];
				vectorPointsForUpper2DProfile = [];
			//}	
			this.pointToBeAddedForLower2DProfile = [];
			this.pointToBeAddedForUpper2DProfile = [];
			/*if(this.pathPointsForLower2DProfile.length > 0)
			{*/	
				
				this.pointToBeAddedForLower2DProfile.push([upperToolPathArray])//, parseInt(this.pathPointsForLower2DProfile[counter][1])]);
				
				for(var i = 0; i < this.pointToBeAddedForLower2DProfile.length ; i++ )
				{
					vectorPointsForLower2DProfile.push(this.pointToBeAddedForLower2DProfile[i]);
				}	
				document.getElementById("ShapeOnRectLower").setAttribute("points", vectorPointsForLower2DProfile);
				this.pointToBeAddedForUpper2DProfile.push([lowerToolPath])//, parseInt(this.pathPointsForUpper2DProfile[counter][1])]);
				
				for(var i = 0; i < this.pointToBeAddedForUpper2DProfile.length ; i++ )
				{
					vectorPointsForUpper2DProfile.push(this.pointToBeAddedForUpper2DProfile[i]);
				}
				
				document.getElementById("ShapeOnRectUpper").setAttribute("points", vectorPointsForUpper2DProfile);
				//this.PopulateLatheProfile(vectorPoints);
				this.counter++;
			/*}
			else
			{
				document.getElementById("ShapeOnRectLower").setAttribute("points", vectorPointsForLower2DProfile);
				document.getElementById("ShapeOnRectUpper").setAttribute("points", vectorPointsForUpper2DProfile);
			}
		}*/
		/*if(this.stopFlag == true)
		{
			this.counter = 0;
			this.delayArray = [];
			this.pathPointsForLower2DProfile = [];
			this.pathPointsForUpper2DProfile = [];
			this.pointToBeAddedForLower2DProfile = [];
			this.pointToBeAddedForUpper2DProfile = [];
			this.pauseFlag = false;
			document.getElementById("ShapeOnRectLower").setAttribute("points", "");
			document.getElementById("ShapeOnRectUpper").setAttribute("points", "");
		}*/	
		
	}
};

function sleep(ms) {
	  return new Promise(resolve => setTimeout(resolve, ms || 1000));
}
