var toolPathCsvData = [];	
function ReadCSV(file)
{
	var reader = new FileReader();
	if (file != null && file != undefined) 
	{
		reader.readAsText(file);
		reader.onload = function(event) 
		{
			var csv = event.target.result;
			var title = csv.split(",");
			var Matrix = [];
			Matrix = listToMatrix(title, 2);
			//toolPathCsvData = Matrix;
		}
	}
}

function listToMatrix(list, elementsPerSubArray) 
{
		var matrix =[], i, k;

		for (i = 0, k = -1; i < list.length; i++) {
			if (i % elementsPerSubArray === 0) {
				k++;
				matrix[k] = [];
			}
			matrix[k].push(list[i]);
		}
		return matrix;
}

function convertCSVToArray(str, delimiter = ',') 
{
		var title = str.split(delimiter);
		var Matrix = [];
		Matrix = listToMatrix(title, 2);
		return  Matrix ;
}

	
toolPathCsvData.push([0,0,30,0,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,30,3,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,30,10,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,30,60,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,20,60,30,60,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,20,60,30,80,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,20,60,30,80,30,90,30,140,0,140]);
toolPathCsvData.push([0,0,30,0,30,3,29,3,3,10,3,60,20,60,30,80,30,90,23,140,0,140]);

