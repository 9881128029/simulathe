import Engine from './engine.js';
import cutPolygon from '../../Code/Maths/cutPolygon.js';
import AnimaterFor2D from './2D_animater.js';
import mathUtils from '../../Code/Maths/MathUtils.js';
export default class commandProcessor
{
	constructor(engine)
	{
		this.engine = engine;
		this.cutPolygonObj = "";
		this.toolArray = [];
		this.vectorPointsBase = [];
		this.distanceBetweenTwoPoints = 0;
		this.previousPoint = [];
	}
	
	DefineStock(diameter, length, colour)
	{
		this.vectorPointsBase = [0,0, length, diameter/2]
	}
	
	DefineTool(type, array, colour)
	{
		this.toolArray = array;
		this.engine.PopulateToolProfile(array);
	}
	
	async MoveToPosition(point, speed)//(x, y, speed, type, arrayData)
	{
		if(this.previousPoint != "")
		{	
		var toolPosition = point;
		this.distanceBetweenTwoPoints = this.DistanceBetweenTwoPoints(this.previousPoint[0], toolPosition);
		let utils = new mathUtils();
		let interpolatedArray = utils.InsertPoints(this.previousPoint[0], toolPosition, parseInt(this.distanceBetweenTwoPoints/1));
		var height = document.getElementById('svgArea').clientHeight;
		var width = document.getElementById('svgArea').clientWidth;
		var dataWidth = 28;
		var dataHeight = 141;
		var AninmaterFor2DProfile = new AnimaterFor2D();
		
		for(var i = 0; i < interpolatedArray.length-1; i++)
		{
			var points = [interpolatedArray[i],interpolatedArray[i+1]];
			var unifiedPolygons = this.cutPolygonObj.GetFinalCutPath(points);
			this.engine.UpdateToolPath(unifiedPolygons, points, 0);
			var transformedData = transformPoints(unifiedPolygons, dataHeight,dataWidth, width, height);
			AninmaterFor2DProfile.Update2DToolPath(transformedData[0][0], transformedData[1][0], 0);
		}	
		this.previousPoint = [point];
		}
		else
		{
			this.previousPoint = [point];
		}	
		/*var unifiedPolygons = this.cutPolygonObj.GetFinalCutPath(point);
		await this.engine.UpdateToolPath(unifiedPolygons, toolPosition, delay);
		
		var height = document.getElementById('svgArea').clientHeight;
		var width = document.getElementById('svgArea').clientWidth;
		var dataWidth = 28;
		var dataHeight = 141;
		var AninmaterFor2DProfile = new AnimaterFor2D();
		var transformedData = transformPoints(unifiedPolygons, dataHeight,dataWidth, width, height);
		await AninmaterFor2DProfile.Update2DToolPath(transformedData[0][0], transformedData[1][0], delay);*/
	}
	
	InitializeMathsModule()
	{
		for(var i = 0; i< this.toolArray.length;i++ )
			{
			this.toolArray[i][1] *= -1;
			}
		this.cutPolygonObj = new cutPolygon(this.vectorPointsBase, this.toolArray, 0); 
	}
	
	DistanceBetweenTwoPoints(point1, point2)
	{
		var distance = Math.sqrt( (Math.pow((point2[0]-point1[0]), 2))+ (Math.pow((point2[1]-point1[1]), 2)) );
		return distance;
	}
	
}

