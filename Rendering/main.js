import contentControl from './contentControl.js';
var csvData =[];
var ContentControl = 0;
var dataWidth = 0;
var dataHeight = 0;
function main() 
{
	ContentControl =  new contentControl();
	ContentControl.Init();
	dataWidth = 28;
	dataHeight = 141
	Initialize2DArea(4 ,1 , dataWidth, dataHeight);
	
}
main();
function Initialize2DArea(x,y,width,height)
{
	var points = [[x,y], [x+ width*2, y], [x+width*2, y + height], [x, y + height], [x,y] ];
	var svgHeight = document.getElementById('svgArea').clientHeight;
	var svgWidth = document.getElementById('svgArea').clientWidth;
	//var transformedData = transformPoints(points,dataHeight,dataWidth*2, svgWidth,svgHeight);
	//document.getElementById("ShapeOnRectLower").setAttribute("points", transformedData[0]);
	//document.getElementById("ShapeOnRectUpper").setAttribute("points", transformedData[1]);
	
}
$("#csvFile").change(function(event) {
	   // $(event.target).data('changed',true);
	toolPathCsvData = [];
	var file = document.querySelector('#csvFile').files[0];
	ReadCSV(file);
	});

$("#idOk").click(function(event) {
	ContentControl.InitializeCommandProcessor();
	var height = document.getElementById('svgArea').clientHeight;
	var width = document.getElementById('svgArea').clientWidth;
	var transformedData = transformPoints(toolPathCsvData, dataHeight,dataWidth, width, height);
	toolPathDataFor2D = transformedData;
	ContentControl.AnimationFor2D(toolPathDataFor2D);
	//document.getElementById("ShapeOnRectLower").setAttribute("points", toolPathDataFor2D[0]);
	//document.getElementById("ShapeOnRectUpper").setAttribute("points", toolPathDataFor2D[1]);
});
$("#playButtonId").click(function(event) {
	ContentControl.PlayAnimation();
});

$("#pausetButtonId").click(function(event) {
	ContentControl.PauseAnimation();
});

$("#stopButtonId").click(function(event) {
	ContentControl.StopAnimation();
});


/*  const canvas = document.querySelector('#c');
  const renderer = new THREE.WebGLRenderer({canvas});

  const fov = 75;
  const aspect = 2;  // the canvas default
  const near = 0.1;
  const far = 1000;
  var camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
  var controls = new OrbitControls(camera, renderer.domElement);
  camera.position.y = 50;
  camera.position.z = 50 ;
  camera.position.x = 50 ;
 
  const scene = new THREE.Scene();
  {
	    const color = 0xFFFFFF;
	    const intensity = 1;
	    const light = new THREE.DirectionalLight(color, intensity);
	    light.position.set(-20, 0, 0);
	    scene.add(light);
	    
	    const light2 = new THREE.DirectionalLight(color, intensity);
	    light2.position.set(20, 0, 0);
	    scene.add(light2);
	    
	    const ambientLight = new THREE.AmbientLight(color, intensity)
	    scene.add(ambientLight);
  }
 
  const radialSegments = 15;
  const material = new THREE.MeshStandardMaterial
  ({
	    color: 0x888888,
  		roughness:0.6,
  		metalness:0.6
  });
 
 var geometries = [];
 let arrOfDimensions = [
             [15,10],
             [10,8],
             [6,5],
             [2,5]
            ];            //cutDepth and cutLength pairs in a 2D array

 makeInstance(40, 20, arrOfDimensions, 30);
 
function makeInstance(Height, radius, arrOfDimensions, radiusOfBase)
{
  
  var outerCylinderPos = 0; 
  var height = Height;
 
  const radiusTop = radius;
  const radiusBottom = radius;
  var geometry = new THREE.CylinderGeometry(radiusTop, radiusBottom, height, radialSegments);
  var cylinder = new THREE.Mesh(geometry, material);
  
  const baseRadius = radiusOfBase;
  const baseHeight = 10;
  var baseGeometry = new THREE.CylinderGeometry(baseRadius, baseRadius, baseHeight, radialSegments);
  var base = new THREE.Mesh(baseGeometry, material);
  base.position.y = -height/2 - 5;
  
  let dimensions= arrOfDimensions;
  const size= dimensions.length;
  var totalHeight=0;
  var validity= 1;
  var i;
  for (i=0; i<size; i++)
	  {
	     totalHeight += dimensions[i][1];
	     
	     if( (dimensions[i][0]>radius && dimensions[i][1]>height) || totalHeight>height)
	    	 {
	    	   validity = -1;
	    	 }
	     	 
	  }
  
  if (validity == 1)
	  {
	  var totalCut =0;
	    for (i=0; i<size; i++)
	    	{
	    	  if (i == 0)
	    	     geometries[i] = makeInnerCylinder(radiusTop, dimensions[i][0], height);
	    	  else 
	    	  {
	    		  totalCut += dimensions[i][1];
	    		  geometries[i] = makeInnerCylinder(radiusTop, dimensions[i][0], height- totalCut);
	    	  }
	    	
	    	  geometries[i].position.y -=(totalCut)/2;
	    	  scene.add(geometries[i]);
	    	}
	    
	  }
  
  scene.add(cylinder, base);
  
function render(time) 
 {
    time *= 0.001;  // convert time to seconds

    if (resizeRendererToDisplaySize(renderer)) 
        {
	      const canvas = renderer.domElement;
	      camera.aspect = canvas.clientWidth / canvas.clientHeight;
	      camera.updateProjectionMatrix();
	    }
    
    if(validity== 1)
    	{
    	
       if( height > 0 )
           {	
    	     scene.remove(cylinder);
    	     height -= 0.02;
             geometry= new THREE.CylinderGeometry(radiusTop, radiusBottom, height, radialSegments);   
             cylinder = new THREE.Mesh(geometry, material);
             outerCylinderPos -= 0.01;
             cylinder.position.y = outerCylinderPos;
             scene.add(cylinder);
            //cone.position.y -= 0.02;
           }
    
    base.rotation.y = time;
    	}
    
    renderer.render(scene, camera);
    requestAnimationFrame(render);
  }
  requestAnimationFrame(render); 
}
}

function makeInnerCylinder(radius, CutDepth, height)
{     
	  const radialSegments = 15;
	  const material = new THREE.MeshStandardMaterial
	  ({
		    color: 0x228888,
	  		roughness:0.6,
	  		metalness:0.6
	  });
	  
	  var cutDepth = CutDepth;
	  const radiusInner = radius-cutDepth;
	  var geometryInner= new THREE.CylinderGeometry(radiusInner, radiusInner, height, radialSegments);
	  const cylinderInner = new THREE.Mesh(geometryInner, material);
	  return cylinderInner;
}

function resizeRendererToDisplaySize(renderer) 
{
    const canvas = renderer.domElement;
    const pixelRatio = window.devicePixelRatio;
    const width  = canvas.clientWidth  * pixelRatio | 0;
    const height = canvas.clientHeight * pixelRatio | 0;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) 
    {
    
      renderer.setSize(width, height, false);
    }
    return needResize;
} 
*/

