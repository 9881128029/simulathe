


export default class PickInfo {

	constructor()
	{
		this.pickPoints = [2][3];
		this.pickCount = 0;
		this.distance = 0;
	}
	
	pick(pickLocation)
	{
		this.pickPoints[this.pickCount] = pickLocation;
		this.pickCount +=1;
		if(this.pickCount == 2)
		{
			this.distance = 1;
			this.pickCount = 0;
		}
	}
	
};